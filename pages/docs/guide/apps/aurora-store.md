---
title: Aurora Store
description: A Google Playstore Client
icon: /assets/images/apps/com.aurora.store.png
autolink: true
---

<div class="button-bar" markdown="0">
<a class="btn" href="http://auroraoss.com">Home Page</a>
<a class="btn" href="https://gitlab.com/AuroraOSS/AuroraStore">Source Code</a>
<a class="btn" href="https://www.paypal.me/whyorean">Donate</a>
<a class="btn" href="https://gitlab.com/AuroraOSS/AuroraStore/issues">Report a Bug</a>
</div>

This **Aurora Store** is a complete rewrite, replacing https://f-droid.org/packages/com.dragons.aurora.

Aurora Store is an alternate to Google's Play Store, with an elegant design,
using Aurora you can download apps, update existing apps, search for apps,
get details about in-app trackers, spoof your location and much more.
